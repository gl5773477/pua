#!/bin/bash
set -e

basepath=$(cd `dirname $0`; pwd)
cd $basepath
dir=`pwd`
fn=$dir/SUMMARY.md
rm -f $fn

echo -e "# Summary\n">$fn

h="#"
tab=""
idxCount=0


function headAdd(){
    h=$h+"#"
}
function headSub(){
    h=${h:1}
}

function tabAdd(){ 
    tab=`echo "$tab  "`
}
function tabSub(){
    tab=${tab:2}
}

function file_dir() {
    local subdir=$1
    local pardir=$2
    if [ "$3" != true ];then # 是否为一级目录
        tabAdd
    else
        echo -e "\n-----\n">>$fn
    fi

    echo "当前目录：" $pardir
    cd $subdir

    if [ -f "./README.md" ];then #当前目录下是否存在说明
        echo "$tab* [${subdir:2}](${pardir:2}/README.md)">>$fn
    else
        echo "$tab* ${subdir:2}">>$fn
    fi

    # 若存在子目录，则递归
    for d in $(find . -maxdepth 1 -type d -print | sort)
        do
            if [ $d != "." ]; then
                file_dir $d $pardir/${d:2}
            fi
        done
        
    # 文件放在最后
    for f in $(find . -maxdepth 1 -type f -name "*.md" -print | sort)
        do
            if [[ $f =~ "README.md" ]];then
                echo
            else
                tabAdd
                echo "$tab* [${f:2}](${pardir:2}/${f:2})">>$fn
                tabSub
            fi
        done
    
    cd ..
    tabSub
}

for d in $(find . -maxdepth 1 -path "./.git" -prune -o -type d -print)
    do
        if [ $d != "." ] && [ $d != "./node_modules" ] && [ $d != "./_book" ]; then
            # 遍历所有文件和目录，构建summary.md
            echo "父目录：" $d
            file_dir $d $d true
            
            
            # echo ${d:2}
            # echo ${d##*/} "======"
        fi
    done

